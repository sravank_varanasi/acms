var express = require('express');
var md5 = require('md5');
var bodyparser = require('body-parser');
const path = require('path');
var multer = require('multer');
const jwt = require('jsonwebtoken');
var app = express();
var router = express.Router();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

/**Mysql Connection */
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'acms'
});
connection.connect();
/**Mysql Connection */

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});



const DIR = '../src/assets/img/banners';
 
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + '' + path.extname(file.originalname));
    }
});
let upload = multer({storage: storage});
app.get('/api', function (req, res) {
  res.end('file catcher example');
});
 
app.post('/api/upload',upload.single('image'), function (req, res) {
    if (!req.file) {
        //console.log("No file received");
        return res.send({
          success: false
        });
    
      } else {
        //console.log('file received');
        return res.send({
			'name':req.file.filename,
          success: true
        })
      }
});


app.post("/api/login", (req,res) => {
   const email = req.body.email;
   const password = md5(req.body.password);
   const expiry = new Date();
     expiry.setDate(expiry.getDate() + 2);
     const PRIVATE_KEY = 'LOGINKEY123456';
     
   connection.query("SELECT * FROM users WHERE email = '"+req.body.email+"' AND password = '"+md5(req.body.password)+"' LIMIT 1 ", function (error, results, fields) {
   if (error){ throw error; }else{
    if(results.length == 0) {
      res.send({errorMessage :"incorrect email/password"});
    }else{
      const token  = jwt.sign({
        email: email,
        password: password,
        name:results[0].name,
        type:results[0].type,
        id:results[0].id,
        exp: parseInt(expiry.getTime() / 1000),
      }, PRIVATE_KEY); // DO NOT KEEP YOUR SECRET IN THE CODE!
      connection.query("UPDATE users SET token = '"+token+"' WHERE email = '"+req.body.email+"' ", function (error, result, fields) {
    res.send({'result': results, 'token' : token, 'type': results[0].type});
      });
    }
    }
});
});


app.get("/api/user/:token", (req,res) => {
  var token = req.params.token;
  connection.query("SELECT * FROM users WHERE token = '"+req.params.token+"'  LIMIT 1 ", function (error, result, fields) {
    if (error){ throw error; }else{
      if(result.length == 0) {
      }else{
        console.log(result);
        res.send({'result': result });
      }
    }
  });
});

var user = require("./user/user");
app.use("/api/user/", user);
var fetch = require("./user/fetch");
app.use("/api/getusers/", fetch);
var emailfetch = require("./user/fetch-email");
app.use("/api/emailfetch/", emailfetch);
var idfetch  = require("./user/fetch-id");
app.use("/api/iduser/", idfetch);
var deleteuser  = require("./user/delete");
app.use("/api/userdelete/", deleteuser);

var banner = require("./banner/banner");
app.use("/api/banner/", banner);
var bannerfetch = require("./banner/fetch");
app.use("/api/getbanners/", bannerfetch);
var idbannerfetch  = require("./banner/fetch-id");
app.use("/api/idbanner/", idbannerfetch);
var deletebanner  = require("./banner/delete");
app.use("/api/bannerdelete/", deletebanner);
app.listen(1234);
console.log("Server Listening the port no.1234");