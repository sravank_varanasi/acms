/**Mysql Connection */
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'acms'
});
connection.connect();
module.exports = connection;
/**Mysql Connection */