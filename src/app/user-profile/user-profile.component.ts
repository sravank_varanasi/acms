import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './../services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ValidateEmailNotTaken } from '../validators/async-email-not-taken.validator';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userForm: FormGroup;
  userupdateForm: FormGroup;
  submitted = false;
  invalidLogin: boolean;
  loading = false;
  errorMsg: string;
  successMsg: string;
  action: any;
  usersData: any;
  editData: any;
  name: string;
  email: string;
  password: string;
  type: string;
  constructor( private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder,
    private userService: UserService) {
      this.userForm = formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.minLength(6)]],
        type: ['', Validators.required],
        id: ['']
    });
     // this.formControlValueChanged();
  }

  ngOnInit() {
    this.editData = { id: null, name: null, email: null, password: null} ;
        this.usersData = this.userService.getLoginUserDetails();
          this.userService.getUsersById(this.usersData.id).subscribe(res => {
            this.editData = res.json();
            this.editData = this.editData[0];
          }, (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('Client side Error !');
            } else {
              console.log('Server side Error !' + err);
            }
          });
     this.userForm.controls['email'].setAsyncValidators(ValidateEmailNotTaken.createValidator(this.userService, this.editData.id));

 }
 get f() { return this.userForm.controls; }
 onSubmit() {
     this.submitted = true;

     // stop here if form is invalid
     if (this.userForm.invalid) {
         return;
     }
     const form = this.userForm.value;
     this.update(this.userForm.value);
 }
  update(credentials) {
    this.loading = true;
    this.userService.update(credentials)
      .subscribe(result => {
        if (result.status) {
          this.successMsg = result.status;
          this.loading = false;
          this.router.navigate(['profile']);
        } else {
          this.invalidLogin = true;
          this.errorMsg = result.errorMessage;
          this.loading = false;
        }
      });
  }

}
