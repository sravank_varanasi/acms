import { AdminAuthService } from './../services/admin-auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    invalidLogin: boolean;
    loading = false;
    errorMsg: string;
    constructor( private router: Router, private route: ActivatedRoute,
      private formBuilder: FormBuilder, private authService: AdminAuthService) {

    }

    ngOnInit() {
        this.authService.logout();
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
    get isFormValid() { return !this.loginForm.invalid; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.signIn(this.loginForm.value);
    }

    signIn(credentials) {
      this.loading = true;
      this.authService.login(credentials)
        .subscribe(result => {
          if (result.token) {
            if (result.type === 'Admin') {
            this.router.navigate(['admin/dashboard']);
            } else if (result.type === 'User') {
              this.router.navigate(['profile']);
            }
            this.authService.setLoggedIn(true);
          } else {
            this.invalidLogin = true;
            this.errorMsg = result.errorMessage;
            this.loading = false;
          }
        });
  }
}
