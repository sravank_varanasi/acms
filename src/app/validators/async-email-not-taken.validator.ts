import { AbstractControl } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import 'rxjs/add/operator/map';

export class ValidateEmailNotTaken {
  static createValidator(userService: UserService, customerId: string) {
    return (control: AbstractControl) => {
      return userService.getUsersByEmail(control.value, customerId).map(res => {
        return res.json().emailNotTaken ? {emailTaken: true} : null;
      });
    };
  }
}
