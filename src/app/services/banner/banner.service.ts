import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private _http: Http, private http: HttpClient, private router: Router) { }
    insert(data) {
    return this._http.post('/api/banner/insert', data)
       .map(res => {
         if (res.json().errorMessage) {
          return res.json();
         } else {
        return  res.json();
         }
        }).catch(this._handleError);

  }
  update(data) {
    return this._http.post('/api/banner/update', data)
       .map(res => {
         if (res.json().errorMessage) {
          return res.json();
         } else {
        return  res.json();
         }
        }).catch(this._handleError);
  }
  delete(id) {
    return this._http.delete('/api/bannerdelete/' + id)
    .map(res => {
      if (res.json().errorMessage) {
       return res.json();
      } else {
     return  res.json();
      }
     }).catch(this._handleError);
  }
  getBanners() {
    return this.http.get('/api/getbanners/')
    .map(res => {
      const result =  res;
      return result;
    });
  }
  getbannersByEmail(email, id) {
    return this._http.get('/api/emailfetch/' + email + '/' + id)
    .map(res => {
      const result =  res;
     return result;
    });
  }
  getBannersById(id) {
    return this._http.get('/api/idbanner/' + id)
    .map(res => {
      const result =  res;
      return result;
    });
  }
  public _handleError(err) {
    console.error('Error Raised....' + err);
    return Observable.throw(err || 'Internal Server Error');
  }
}
