import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
@Injectable({
  providedIn: 'root'
})
export class AdminAuthService {
  private token: string;
  public loggedInStatus = false;
    constructor(private _http: Http, private http: HttpClient, private router: Router, public jwtHelper: JwtHelperService) {
     }
      login(credentials) {
       return this._http.post('/api/login', credentials)
       .map(res => {
         if (res.json().errorMessage) {
          return res.json();
         } else {
        localStorage.setItem('currentUserToken', res.json().token);
        return { 'token' : res.json().token , 'type' : res.json().type} ;
         }
      }).catch(this._handleError);
    }
      getUserDetails() {
        return this._http.get('/api/data');

    }
      isLoggedIn(): Observable<IsLoggedIn> {
      return this._http.get('/api/isloggedin').map(res => {
        return res.json();
      });
    }
    public isAuthenticated(): boolean {
      const token = localStorage.getItem('currentUserToken');
      // Check whether the token is expired and return
      // true or false
      return !this.jwtHelper.isTokenExpired(token);
    }
    setLoggedIn(value: boolean) {
      this.loggedInStatus = value;
    }

    getCurrentUser(): any {
      const token = localStorage.getItem('currentUserToken');
      return this._http.get('/api/user' + token).map( res => {
        console.log(res);
        return res.json();
      });
    }
     saveToken(token: string): any {
      const body =  { 'token': token };
      // this.token = token;
    }

     getToken(): string {
      return localStorage.getItem('currentUserToken');
    }

    logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('currentUserToken');
      this.router.navigate(['/login']);

  }
     public _handleError(err) {
      console.error('Error Raised....' + err);
      return Observable.throw(err || 'Internal Server Error');
    }
  }
  export interface UserDetails {
    _id: string;
    email: string;
    name: string;
    exp: number;
    iat: number;
  }

  interface TokenResponse {
    token: string;
  }
  interface IsLoggedIn {
    status: boolean;
  }
  export interface TokenPayload {
    email: string;
    password: string;
    name: string;
  }
