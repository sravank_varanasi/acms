import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { JwtModule } from '@auth0/angular-jwt';
import { DataTablesModule } from 'angular-datatables';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdminComponent } from './admin/admin.component';
import { AdminHeaderComponent } from './_layout/admin-header/admin-header.component';
import { AdminSidebarComponent } from './_layout/admin-sidebar/admin-sidebar.component';
import { AdminFooterComponent } from './_layout/admin-footer/admin-footer.component';
import { UserHeaderComponent } from './_layout/user-header/user-header.component';
import { UserFooterComponent } from './_layout/user-footer/user-footer.component';
import { AdminLayoutComponent } from './_layout/admin-layout/admin-layout.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserComponent } from './user/user.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

import { PageNotFoundComponent } from './_layout/page-not-found/page-not-found.component';
import { UserLayoutComponent } from './_layout/user-layout/user-layout.component';
import { UserSidebarComponent } from './_layout/user-sidebar/user-sidebar.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { BannerComponent } from './banner/banner.component';
import { RemoveHypenPipe } from './pipe/removeHypen.pipe';
import { FileSelectDirective } from 'ng2-file-upload';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    UserHeaderComponent,
    UserFooterComponent,
    AdminLayoutComponent,
    UserProfileComponent,
    UserComponent,
    AdminDashboardComponent,
    PageNotFoundComponent,
    UserLayoutComponent,
    UserSidebarComponent,
    ConfirmationDialogComponent,
    BannerComponent,
    RemoveHypenPipe,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    DataTablesModule,
    JwtModule.forRoot({
      config: {
        whitelistedDomains: ['example.com'],
        blacklistedRoutes: ['example.com/examplebadroute/'],
        tokenGetter: () => {
          return localStorage.getItem('currentUserToken');
        }
      }
    }),
    NgbModule.forRoot()
  ],
  providers: [],
  entryComponents: [ ConfirmationDialogComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
