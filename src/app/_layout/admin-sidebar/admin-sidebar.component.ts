import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.css']
})
export class AdminSidebarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  menuClickHandler(route) {
    this.router.navigate([route]);
  }
  isActive(instruction: string): boolean {
    return this.router.url === instruction;
  }
}
