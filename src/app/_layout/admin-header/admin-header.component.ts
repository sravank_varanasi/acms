import { Component, OnInit } from '@angular/core';
import { AdminAuthService } from 'src/app/services/admin-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {
  authSer;
  constructor(private authService: AdminAuthService, private router: Router) {
    this.authSer = this.authService;
  }
  menuClickHandler(route) {
    this.router.navigate([route]);
  }
  ngOnInit() {
  }

}
