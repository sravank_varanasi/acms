import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-sidebar',
  templateUrl: './user-sidebar.component.html',
  styleUrls: ['./user-sidebar.component.css']
})
export class UserSidebarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  menuClickHandler(route) {
    this.router.navigate([route]);
  }
  isActive(instruction: string): boolean {
    return this.router.url === instruction;
  }


}
