import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'removeHypen' })
export class RemoveHypenPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return value.replace(/-/g, ' ');
  }
}
