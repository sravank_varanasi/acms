import { Component, OnInit } from '@angular/core';
import { AdminAuthService } from '../services/admin-auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  users;
  constructor(private admin: AdminAuthService) {
  }

  ngOnInit() {
  }

}
