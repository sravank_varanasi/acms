import { Component, OnDestroy, OnInit  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BannerService } from './../services/banner/banner.service';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { TransferService } from './../services/transfer.service';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})

export class BannerComponent implements OnDestroy, OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  banners: any;

  bannerForm: FormGroup;
  bannerupdateForm: FormGroup;
  submitted = false;
  invalidLogin: boolean;
  loading = false;
  errorMsg: string;
  successMsg: string;
  action: any;
  bannersData: any;
  editData: any;
  name: string;
  email: string;
  password: string;
  type: string;
  constructor( private router: Router, private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private transferService: TransferService,
    private http: HttpClient,
    private confirmationDialogService: ConfirmationDialogService,
    private bannerService: BannerService) {
      this.route.params.subscribe(params => this.action = params);
      this.route.params.subscribe(id => this.action = id);
     // this.formControlValueChanged();
  }

  selectedFile = null;
  imagepath = null;
  public result;
  public dat: any = [ {'name': '', 'banner': '', 'page' : ''} ];
  public uploader: FileUploader = new FileUploader({url: '/api/upload', itemAlias: 'image'});

  onFileSelected(event) {
    this.uploader.uploadAll();
      }
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      responsive: true
    };
    this.http.get('/api/getbanners/')
      .map(this.extractData)
      .subscribe(banners => {
        this.banners = banners;
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });
     this.bannerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')]],
      banner: ['', Validators.required],
      page: ['', Validators.required],
      id: [this.action['id']]
  });
  this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
        this.selectedFile = JSON.parse(response);
        this.imagepath = this.selectedFile.name;
         this.f.banner.patchValue(this.selectedFile.name);
     };
    this.successMsg = this.transferService.getData();
     this.bannersData = this.bannerService.getBanners();
     this.editData = { id: null, name: null, banner: null, page: null} ;
     if (this.action['id']) {
        this.bannerService.getBannersById(this.action['id']).subscribe(res => {
          this.editData = res.json();
          this.editData = this.editData[0];
          this.imagepath = this.editData.image;
        }, (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('Client side Error !');
          } else {
            console.log('Server side Error !' + err);
          }
        });
      }

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res) {
    const body = res;
    return body || {};
  }

  /* formControlValueChanged() {
    const passControl = this.bannerForm.get('password');
    if (this.action['id'] !== null) {
    } else {
    passControl.setValidators([Validators.required]);
    }
    } */

  get f() { return this.bannerForm.controls; }
  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.bannerForm.invalid) {
          return;
      }
    if (this.action['id'] != null) {
      this.update(this.bannerForm.value);
    } else {
    this.insert(this.bannerForm.value);
    }
  }

  /* uniqueEmail() {
    const emailControl = this.bannerForm.get('email');
    this.bannerService.getbannersByEmail(emailControl.value, this.action['id']).subscribe(result => {
      if(result === true){
        emailControl.setValidators([Validators.emailTaken]);
      }
    });
  } */
  insert(credentials) {
    this.loading = true;
    const fd  = new FormData();
    fd.append('url', this.selectedFile, this.selectedFile.name);
     this.bannerService.insert(credentials)
      .subscribe(result => {
        if (result.status) {

          this.successMsg = result.status;
          this.loading = false;
          this.transferService.setData(this.successMsg);
          this.router.navigate(['banner']);
        } else {
          this.invalidLogin = true;
          this.errorMsg = result.errorMessage;
          this.loading = false;
        }
      });
}
update(credentials) {
  this.loading = true;
  this.bannerService.update(credentials)
    .subscribe(result => {
      if (result.status) {

        this.successMsg = result.status;
        this.transferService.setData(this.successMsg);
        this.loading = false;
        this.router.navigate(['banner']);
      } else {
        this.invalidLogin = true;
        this.errorMsg = result.errorMessage;
        this.loading = false;
      }
    });
}
public deletebanner(id) {
  const result = this.confirmationDialogService.confirm('Please confirm to delete banner',
  'Do you really want to delete banner ?', 'Delete', 'Cancel', 'lg').then(
    (confirmed) => {
    if (confirmed === true) {
this.bannerService.delete(id).subscribe(res => {
  this.successMsg = res.status;
  this.bannersData = this.bannerService.getBanners();
  this.router.navigate(['banner']);
}, (err: HttpErrorResponse) => {
  if (err.error instanceof Error) {
    console.log('Client side Error !');
  } else {
    console.log('Server side Error !');
  }
});
    }
  });
}

menuClickHandler(route) {
  console.log(route);
  this.router.navigateByUrl(route);
}


}
