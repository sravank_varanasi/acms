import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PageNotFoundComponent } from './_layout/page-not-found/page-not-found.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AdminLayoutComponent } from './_layout/admin-layout/admin-layout.component';
import { UserLayoutComponent } from './_layout/user-layout/user-layout.component';
import { UserComponent } from './user/user.component';
import { RoleGuardService } from './services/role-guard.service';
import { BannerComponent } from './banner/banner.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
      path: '',
      component: UserLayoutComponent,
      children: [
    { path: 'profile', component: UserProfileComponent, pathMatch: 'full', canActivate: [AuthGuardService]},
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    children: [
      { path: 'admin', component: AdminComponent, pathMatch: 'full', canActivate: [AuthGuardService]},
      { path: 'dashboard', component: AdminDashboardComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      }},
      { path: 'user', component: UserComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      }},
      { path: 'user/:params', component: UserComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      }},
      { path: 'user/:params/:id', component: UserComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      } },
      { path: 'banner', component: BannerComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      }},
      { path: 'banner/:params', component: BannerComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      }},
      { path: 'banner/:params/:id', component: BannerComponent, pathMatch: 'full', canActivate: [RoleGuardService],
      data: {
        expectedRole: 'Admin'
      } },
      { path: '404', component: PageNotFoundComponent, pathMatch: 'full', canActivate: [AuthGuardService]}
    ],
    canActivate: [AuthGuardService]
},
 { path: '', component: LoginComponent },
{ path: '**', component: NotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
